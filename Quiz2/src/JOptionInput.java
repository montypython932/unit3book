import javax.swing.JOptionPane;


public class JOptionInput {
	
	public static void main(String[] args) {
		Quiz2 quiz = new Quiz2();
		String username = JOptionPane.showInputDialog("Please enter your Username: ");
		if (quiz.authenticateUsername(username) == true) {
			String password = JOptionPane.showInputDialog("Please enter your password: ");
			if (quiz.authenticatePassword(password) == true) {
				JOptionPane.showMessageDialog(null, "Welcome, " + username);
			} else {
				JOptionPane.showMessageDialog(null, "Invalid Password!");
			}
		} else {
			JOptionPane.showMessageDialog(null, "Invalid Username");
		}
	}
}
