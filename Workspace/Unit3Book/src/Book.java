public class Book {
	
	String title,
		   author;
	int numberOfPages;

	public Book() {
		
	}
	
	public Book(String title, String author, int numberOfPages) {
		this.title = title;
		this.author = author;
		this.numberOfPages = numberOfPages;
	}
	
	public String toString() {
		return "Title: " + title + "\nAuthor: " + author + "\nNumber Of Pages: " + numberOfPages + "\n\n";
	}
}
