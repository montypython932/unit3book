
public class Volume {
	String volumeName;
	byte numberOfBooks;
	Book books[];
	
	public Volume() {
		
	}
	
	public Volume(String volumeName, byte numberOfBooks, Book books[]) {
		this.volumeName = volumeName;
		this.numberOfBooks = numberOfBooks;
		this.books = books;
	}
	
	public void getBookArray() {
		boolean printedVolume = false;
		for(int i = 0; i < books.length; i++) {
			if (printedVolume == false) {
				System.out.println(toString());
				printedVolume = true;
			}
			System.out.println(books[i].toString());
		}
	}
	
	public String toString() {
		return "Volume Name: " + volumeName + "\nNumber Of Books: " + numberOfBooks + "\nBooks: ";
	}
}
